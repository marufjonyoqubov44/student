<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $username
 * @property int $question_id
 * @property int $error_type
 * @property string $status
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'question_id', 'error_type'], 'required'],
            [['student_id', 'question_id', 'error_type'], 'integer'],
            [['status'], 'integer'],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'question_id' => 'Question ID',
            'error_type' => 'Error Type',
            'status' => 'Status',
        ];
    }
}
