<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name
 * @property int $profile_id
 * @property string|null $description
 * @property string $created_at
 * @property string $status
 * @property string $link
 */
class Group extends \yii\db\ActiveRecord
{
    const ACTIVE = 'ACTIVE';
    const DELETED = 'DELETED';
    const ARCHIVED = 'ARCHIVED';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // ['name','required', 'message' => 'name is required'],  return message
            [['name', 'profile_id', 'created_at', 'status', 'link'], 'required'],
            [['profile_id'], 'integer'],
            [['description', 'status'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['created_at'], 'string', 'max' => 20],
            [['link'], 'string', 'max' => 100],
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'profile_id' => 'Profile ID',
            'description' => 'Description',
            'created_at' => 'Created At',
            'status' => 'Status',
            'link' => 'Link',
        ];
    }
    public function getActiveGroups()
    {
        return self::find()->where(['=', 'profile_id', $this->profile_id])->andhere(['=', 'status', self::ACTIVE])->get();
    }
}
