<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "login".
 *
 * @property int $id
 * @property string $email
 * @property string $phone
 * @property int $status 1=> activ  0=>delete
 * @property int $code
 */
class Login extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'code'], 'required'],
            [['status', 'code'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'phone' => 'Phone',
            'status' => 'Status',
            'code' => 'Code',
        ];
    }
}
