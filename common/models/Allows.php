<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "allows".
 *
 * @property int $id
 * @property string $role
 * @property string $controller
 * @property string $action
 * @property int $ombor_id
 */
class Allows extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'allows';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role', 'controller', 'action', 'ombor_id'], 'required'],
            [['ombor_id'], 'integer'],
            [['role'], 'string', 'max' => 10],
            [['controller', 'action'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'controller' => 'Controller',
            'action' => 'Action',
            'ombor_id' => 'Ombor ID',
        ];
    }
}
