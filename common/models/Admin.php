<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property int $id
 * @property int $user_id
 * @property string $phone username bu telefon
 * @property string $full_name
 * @property string|null $birthday
 * @property int|null $gender
 * @property string|null $email
 * @property int|null $address_id
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'phone', 'full_name'], 'required'],
            [['user_id', 'gender', 'address_id'], 'integer'],
            [['birthday'], 'safe'],
            [['phone'], 'string', 'max' => 9],
            [['full_name', 'email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'phone' => 'Phone',
            'full_name' => 'Full Name',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'email' => 'Email',
            'address_id' => 'Address ID',
        ];
    }
}
