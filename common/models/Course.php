<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string $name
 * @property string|null $code
 * @property int $subject1
 * @property int $subject2
 * @property int $subject3
 * @property int $subject4
 * @property int $subject5
 * @property string $status
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'subject1', 'subject2', 'subject3', 'subject4', 'subject5'], 'required'],
            [['subject1', 'subject2', 'subject3', 'subject4', 'subject5'], 'integer'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'subject1' => 'Subject1',
            'subject2' => 'Subject2',
            'subject3' => 'Subject3',
            'subject4' => 'Subject4',
            'subject5' => 'Subject5',
            'status' => 'Status',
        ];
    }
}
