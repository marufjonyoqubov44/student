<?php

namespace frontend\controllers;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use common\models\Address;
use common\models\Language;
use common\models\Lesson;
use common\models\Option;
use common\models\Question;
use common\models\Profile;
use common\models\Subject;
use common\models\Course2;
use common\models\UserIp;
use common\models\Login;
use common\models\Error;
use common\models\Feedback;
use common\models\Group;
use common\models\Seo;
use common\models\SiteAnswer;
use common\models\SiteOption;
use common\models\SiteQuestion;
use common\models\StudentGroup;
use common\models\Test;
use common\models\Ticket;
use common\models\User;
use common\models\UserAnswer;
use common\models\Year;
use DateTime;
use DateTimeZone;
use PhpOffice\PhpWord\IOFactory;
use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UploadedFile;

class ClientController extends Controller
{
    // Cors policy and check actions
    public function actionSubjectname()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = $this->cleartext(Yii::$app->request->get('id'));
        if (Yii::$app->request->isGet) {
            $subject = Subject::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->all();
            $response = [
                'subject' => $subject,
            ];
            return $response;
        }
        $this->error_400();
    }
    public function actionSubject()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $name = Yii::$app->request->get('name');
        $size = Yii::$app->request->get('size');
        if (!$size) {
            $size = 20;
        }
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');

        if (Yii::$app->request->isGet) {
            if (isset($token) && $token != '') {
                $student = Profile::find()
                    ->where(['user_id' => $this->Token($token)])
                    ->one();
                if ($student) {
                    $ids = explode(',', $student->test);
                }
            }
            $json = [];
            //kerak
            // $user = User::find()
            // $student = Profile::find()
            // ->where(['user_id'=>82])
            // ->one();
            // $is = explode(',',$student->test);
            // return $is;

            if ($name) {
                $count = Test::find()
                    ->leftJoin('subject', 'subject.id=test.subject_id')
                    ->where(['test.type' => 0])
                    ->andWhere(['test.status' => 1])
                    ->andWhere(['subject.name' => "$name"])
                    ->count();

                $pagination = new Pagination([
                    'totalCount' => $count,
                    'defaultPageSize' => $size,
                ]);

                $lessons = Subject::find()
                    ->select(
                        'lesson.id as id, test.view_count as view_count , subject.name as subject_name,test.create_date, lesson.name as lesson_name, test.id as test_id,'
                    )
                    ->leftJoin('lesson', 'subject.id=lesson.subject_id')
                    ->leftJoin('test', 'test.lesson_id=lesson.id')
                    ->where(['test.status' => 1])
                    ->andWhere(['lesson.status' => 1])
                    ->andWhere(['subject.status' => 1])
                    ->andWhere(['subject.name' => "$name"])
                    ->orderBy(['test.view_count' => SORT_DESC])
                    ->asArray()
                    ->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
                if ($lessons) {
                    foreach ($lessons as $lesson) {
                        $object = (object) [];
                        $count = Question::find()
                            ->where(['test_id' => (int) $lesson['test_id']])
                            ->andWhere(['status' => 1])
                            ->count();
                        $object->subject_name = $lesson['subject_name'];
                        $object->lesson_name = $lesson['lesson_name'];
                        $object->create_date = $lesson['create_date'];
                        $object->test_id = (int) $lesson['test_id'];
                        $object->test_count = $count;
                        $object->view_count = $lesson['view_count'];

                        $object->is_saved = false;
                        if (isset($ids)) {
                            $object->is_saved = false;
                            if (in_array($object->test_id, $ids)) {
                                $object->is_saved = true;
                            }
                        }
                        $json[] = $object;
                    }
                }
                return $json;
                // $response = [
                //     'test' => $json,
                //     'count' => $count,
                // ];
                // return $response;
            }

            $count = Test::find()
                ->where(['test.type' => 0])
                ->andWhere(['test.status' => 1])
                ->count();

            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => $size,
            ]);

            $lessons = Subject::find()
                ->select(
                    'lesson.id as id, test.view_count as view_count , subject.name as subject_name,test.create_date, lesson.name as lesson_name, test.id as test_id,'
                )
                ->leftJoin('lesson', 'subject.id=lesson.subject_id')
                ->leftJoin('test', 'test.lesson_id=lesson.id')
                ->where(['test.status' => 1])
                ->andWhere(['test.type' => 0])
                ->andWhere(['lesson.status' => 1])
                ->andWhere(['subject.status' => 1])
                // ->groupBy('test.id')
                ->orderBy(['test.view_count' => SORT_DESC])
                ->asArray()
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            if ($lessons) {
                foreach ($lessons as $lesson) {
                    $object = (object) [];
                    $count = Question::find()
                        ->where(['test_id' => (int) $lesson['test_id']])
                        ->andWhere(['status' => 1])
                        ->count();
                    $object->subject_name = $lesson['subject_name'];
                    $object->lesson_name = $lesson['lesson_name'];
                    $object->create_date = $lesson['create_date'];
                    $object->test_id = (int) $lesson['test_id'];
                    $object->test_count = $count;
                    $object->view_count = $lesson['view_count'];

                    $object->is_saved = false;
                    if (isset($ids)) {
                        $object->is_saved = false;
                        if (in_array($object->test_id, $ids)) {
                            $object->is_saved = true;
                        }
                    }
                    $json[] = $object;
                }
            }

            // $response = [
            //     'test' => $json,
            //     'count' => $count,
            // ];
            // return $response;

            return $json;
        } elseif (Yii::$app->request->isPost) {
            if ($this->ActiveToken($token)) {
                $user_data = Yii::$app->request->getRawBody();
                $user_data = json_decode($user_data);
                $student = Profile::find()
                    ->where(['user_id' => $this->Token($token)])
                    ->one();
                if ($user_data->status) {
                    if ($student->test) {
                        $student->test = $student->test . ',' . $user_data->id;
                        $student->save(false);
                        return true;
                    }
                    $student->test = $user_data->id;
                    $student->save(false);
                    return true;
                }
                $tests = explode(',', $student->test);
                $student->test = implode(
                    ',',
                    array_diff($tests, [$user_data->id])
                );
                $student->save(false);
                return true;
            }
            $this->error_400();
        }
        $this->error_400();
    }
    public function actionSubjecttest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isGet) {
            // $subject = Subject::find()
            //     ->select('subject.id ,subject.name')
            //     ->leftJoin('test', 'test.subject_id=subject.id')
            //     ->leftJoin('question', 'question.test_id=test.id')
            //     ->where(['test.status' => 1])
            //     ->where(['test.type' => 0])
            //     ->groupBy('subject.id')
            //     ->all();
            $subject = Subject::find()
                ->select('subject.id,subject.name')
                ->leftJoin('test', 'test.subject_id=subject.id')
                ->where(['subject.status' => 1])
                ->andWhere(['test.status' => 1])
                ->andWhere(['test.type' => 0])
                ->all();
            $response = [
                'subject' => $subject,
            ];
            return $response;
        }
        $this->error_400();
    }
    // DTM
    
    public function actionYear()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isGet) {
            $year = Year::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->all();
            $response = [
                'years' => $year,
            ];
            return $response;
        }
        $this->error_400();
    }
    public function actionLanguage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isGet) {
            $language = Language::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->limit(1)
                ->all();
            $response = [
                'languages' => $language,
            ];
            return $response;
        }
        $this->error_400();
    }
    // Virtual
   
    public function actionDirection()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = (int) $this->cleartext(Yii::$app->request->get('id'));
        if (Yii::$app->request->isGet) {
            $json = [];
            if ($id) {
                $this->validation_id('course2', $id);
                $direction = Course2::find()
                    ->where(['status' => 1])
                    ->andWhere(['id' => $id])
                    ->one();
                $blok1 = Subject::find()
                    ->where(['id' => $direction->subject1])
                    ->one();
                $blok2 = Subject::find()
                    ->where(['id' => $direction->subject2])
                    ->one();
                if ($blok1 && $blok2) {
                    $response = [
                        'blok1_id' => $blok1->id,
                        'blok1_name' => $blok1->name,
                        'blok2_id' => $blok2->id,
                        'blok2_name' => $blok2->name,
                    ];
                    return $response;
                }
                $this->error_400();
            }
            $directions = Course2::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->all();
            foreach ($directions as $direction) {
                $dic = Course2::findOne($direction->id);
                $order_1 = Test::find()
                    ->select('test.id,COUNT(question.id) as count')
                    ->having(['>=', 'count', 30])
                    ->where(['test.status' => 1])
                    ->andWhere(['test.subject_id' => $dic->subject1])
                    ->andWhere(['test.type' => 1])
                    ->leftJoin('question', 'test.id=question.test_id')
                    ->groupBy('test.id')
                    ->asArray()
                    ->one();
                $order_2 = Test::find()
                    ->select('test.id,COUNT(question.id) as count')
                    ->having(['>=', 'count', 30])
                    ->where(['test.status' => 1])
                    ->andWhere(['test.subject_id' => $dic->subject2])
                    ->andWhere(['test.type' => 1])
                    ->leftJoin('question', 'test.id=question.test_id')
                    ->orderBy(new Expression('rand()'))
                    ->groupBy('test.id')
                    ->asArray()
                    ->one();

                if ($order_1 && $order_2) {
                    $json[] = $direction;
                }
            }
            $response = [
                'directions' => $json,
            ];
            return $response;
        }
        $this->error_400();
    }

    public function actionTheredirection()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = (int) $this->cleartext(Yii::$app->request->get('id'));
        if (Yii::$app->request->isGet) {
            $json = [];
            $directions = Course2::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->all();
            foreach ($directions as $direction) {
                $dic = Course2::findOne($direction->id);
                $order_1 = Test::find()
                    ->select('test.id,COUNT(question.id) as count')
                    ->having(['count' => 30])
                    ->where(['test.status' => 1])
                    ->andWhere(['test.subject_id' => $dic->subject1])
                    ->andWhere(['test.lang_id' => 1])
                    ->andWhere(['test.year_id' => 1])
                    ->andWhere(['test.type' => 2])
                    ->leftJoin('question', 'test.id=question.test_id')
                    ->groupBy('test.id')
                    ->asArray()
                    ->one();
                $order_2 = Test::find()
                    ->select('test.id,COUNT(question.id) as count')
                    ->having(['count' => 30])
                    ->where(['test.status' => 1])
                    ->andWhere(['test.subject_id' => $dic->subject2])
                    ->andWhere(['test.lang_id' => 1])
                    ->andWhere(['test.year_id' => 1])
                    ->andWhere(['test.type' => 2])
                    ->leftJoin('question', 'test.id=question.test_id')
                    ->orderBy(new Expression('rand()'))
                    ->groupBy('test.id')
                    ->asArray()
                    ->one();

                if ($order_1 && $order_2) {
                    $json[] = $direction;
                }
            }

            $response = [
                'directions' => $json,
            ];
            return $response;
        }
        $this->error_400();
    }
    // Themed
  
    // Error test
    public function actionErrortype()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isGet) {
            $response = [];
            $error = Error::find()
                ->select('id,name')
                ->where(['status' => 1])
                ->all();
            foreach ($error as $err) {
                $json = (object) [];
                $json->value = $err->id;
                $json->label = $err->name;
                $response[] = $json;
            }
            return $response;
        }
        $this->error_400();
    }
    public function actionFeedback()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        $data = Yii::$app->request->getRawBody();
        $data = json_decode($data);
        if (!isset($data->question_id) && !isset($data->error_id)) {
            $this->error(400, "Ma'lumot nomi xata jo'natilgan");
        }
        $this->validation_id('question', $data->question_id);
        $this->validation_id('error', $data->error_id);
        if ($this->Token($token)) {
            if (Yii::$app->request->isPost) {
                $student = Profile::find()
                    ->select('id')
                    ->where(['user_id' => $this->Token($token)])
                    ->one();
                $feedback = new Feedback();
                $feedback->student_id = $this->cleartext($student->id);
                $feedback->question_id = $this->cleartext($data->question_id);
                $feedback->error_type = $this->cleartext($data->error_id);
                if (isset($data->message)) {
                    $feedback->message = $this->cleartext($data->message);
                }
                if ($feedback->save()) {
                    return true;
                }
                return $this->validation($feedback);
            }
            $this->error_400();
        }
        $this->error_400();
    }
    // Check all test
    public function actionCheckTest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ticket_id = (int) $this->cleartext(
            Yii::$app->request->get('ticket_id')
        );
        $data = Yii::$app->request->getRawBody();
        $data = json_decode($data);
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if (Yii::$app->request->isPost) {
            if ($this->Token($token)) {
                $option_id = $this->cleartext($data->option_id);
                $id = $this->cleartext($data->id);
                $answer = UserAnswer::findOne($id);
                $ticket = Ticket::findOne($answer->ticket_id);
                if ($ticket->type == 0) {
                    $options = explode(',', $answer->options);
                    if (in_array($option_id, $options)) {
                        if ($answer) {
                            $answer->is_right = 0;
                            $answer->is_checked = $option_id;
                            if ($option_id == $answer->is_true) {
                                $answer->is_right = 1;
                            }
                            $answer->save(false);
                            return true;
                        }
                        $this->error(400, "Ma'lumot topilmadi");
                    }
                    $this->error(400, "Ma'lumot topilmadi");
                }
                $counts = explode(',', $ticket->test_ids);
                $count = count($counts);
                if ($count == 5) {
                    $count = 3;
                }
                $options = explode(',', $answer->options);
                if (in_array($option_id, $options)) {
                    if (time() - $ticket->created_at < $count * 3600 + 10) {
                        if ($answer) {
                            $answer->is_right = 0;
                            $answer->is_checked = $option_id;
                            if ($option_id == $answer->is_true) {
                                $answer->is_right = 1;
                            }
                            $answer->save(false);
                            return true;
                        }
                        $this->error(400, "Ma'lumot topilmadi");
                    }
                    $this->error(403, 'Vaqt tugadi');
                }
                $this->error(400, "Ma'lumot topilmadi");
            }
            $this->error_400();
        } elseif (Yii::$app->request->isGet) {
            if ($this->Token($token)) {
                $this->validation_id('ticket', $ticket_id);
                $student = Profile::find()
                    ->where(['user_id' => $this->Token($token)])
                    ->one();
                $ticket = Ticket::find()
                    // ->where(['status' => 0])
                    ->andWhere(['id' => $ticket_id])
                    ->andWhere(['student_id' => $student->id])
                    ->one();
                if ($ticket) {
                    if ($ticket->begin_answer_id > 0) {
                        $min_id = $ticket->begin_answer_id;
                        $max_id = $ticket->begin_answer_id + 150;
                    } else {
                        $min_id =
                            UserAnswer::find()
                            // ->select('min(id)')
                            ->where(['ticket_id' => $ticket->id])
                            ->min('id') + 1;

                        $max_id =
                            UserAnswer::find()
                            // ->select('min(id)')
                            ->where(['ticket_id' => $ticket->id])
                            ->max('id') + 1;
                    }
                    $ticket->status = 1;
                    $ticket->finished_at = time();
                    $ticket->save(false);
                    $i = 1;
                    $test_ids = explode(',', $ticket->test_ids);
                    if ($ticket->type == 2 && count($test_ids) < 4) {
                        $ball1 = 0;
                        $ball2 = 0;
                        $ball3 = 0;
                        foreach ($test_ids as $test_id) {
                            $test = Ticket::find()
                                ->select('COUNT(user_answer.id) as count')
                                ->where(['ticket.id' => $ticket->id])
                                ->andWhere([
                                    'ticket.student_id' => $student->id,
                                ])
                                ->andWhere(['ticket.status' => 1])
                                ->andWhere(['user_answer.is_right' => 1])
                                ->andWhere(['>', 'user_answer.id', $min_id])
                                ->andWhere(['<', 'user_answer.id', $max_id])
                                ->andWhere(['test.id' => $test_id])
                                ->leftJoin(
                                    'user_answer',
                                    'user_answer.ticket_id=ticket.id'
                                )
                                ->leftJoin(
                                    'question',
                                    'user_answer.question_id=question.id'
                                )
                                ->leftJoin('test', 'test.id=question.test_id')
                                ->groupBy('test.id')
                                ->asArray()
                                ->one();
                            if (!$test) {
                                $test['count'] = 0;
                            }
                            if ($i == 1) {
                                $ball1 = 3.1 * $test['count'];
                            } elseif ($i == 2) {
                                $ball2 = 2.1 * $test['count'];
                            } elseif ($i == 3) {
                                $ball3 = 1.1 * $test['count'];
                            }
                            $i++;
                        }
                        $ticket->ball = $ball1 + $ball2 + $ball3;
                        $ticket->blok_1 = $ball1;
                        $ticket->blok_2 = $ball2;
                        $ticket->blok_3 = $ball3;
                        $ticket->save(false);
                        return true;
                    } elseif ($ticket->type == 1 || count($test_ids) > 3) {
                        foreach ($test_ids as $test_id) {
                            $test = Ticket::find()
                                ->select('COUNT(user_answer.id) as count')
                                ->where(['ticket.id' => $ticket->id])
                                ->andWhere([
                                    'ticket.student_id' => $student->id,
                                ])
                                ->andWhere(['ticket.status' => 1])
                                ->andWhere(['user_answer.is_right' => 1])
                                ->andWhere(['>', 'user_answer.id', $min_id])
                                ->andWhere(['<', 'user_answer.id', $max_id])
                                ->andWhere(['test.id' => $test_id])
                                ->leftJoin(
                                    'user_answer',
                                    'user_answer.ticket_id=ticket.id'
                                )
                                ->leftJoin(
                                    'question',
                                    'user_answer.question_id=question.id'
                                )
                                ->leftJoin('test', 'test.id=question.test_id')
                                ->groupBy('test.id')
                                ->asArray()
                                ->one();
                            if (!$test) {
                                $test['count'] = 0;
                            }
                            if ($i == 1) {
                                $ball1 = 2.1 * $test['count'];
                            } elseif ($i == 2) {
                                $ball2 = 2.1 * $test['count'];
                            } elseif ($i == 3) {
                                $ball3 = 2.1 * $test['count'];
                            } elseif ($i == 4) {
                                $ball4 = 3.15 * $test['count'];
                            } elseif ($i == 5) {
                                $ball5 = 3.15 * $test['count'];
                            }
                            $i++;
                        }
                        $ticket->ball =
                            $ball1 + $ball2 + $ball3 + $ball4 + $ball5;
                        $ticket->blok_1 = $ball1;
                        $ticket->blok_2 = $ball2;
                        $ticket->blok_3 = $ball3;
                        $ticket->blok_4 = $ball4;
                        $ticket->blok_5 = $ball5;
                        $ticket->save(false);
                        return true;
                    } elseif ($ticket->type == 0) {
                        $test = Ticket::find()
                            ->select('COUNT(user_answer.id) as count')
                            ->where(['ticket.id' => $ticket->id])
                            ->andWhere(['ticket.student_id' => $student->id])
                            ->andWhere(['ticket.status' => 1])
                            ->andWhere(['user_answer.is_right' => 1])
                            ->andWhere(['>', 'user_answer.id', $min_id])
                            ->andWhere(['<', 'user_answer.id', $max_id])
                            ->andWhere(['test.id' => $ticket->test_ids])
                            ->leftJoin(
                                'user_answer',
                                'user_answer.ticket_id=ticket.id'
                            )
                            ->leftJoin(
                                'question',
                                'user_answer.question_id=question.id'
                            )
                            ->leftJoin('test', 'test.id=question.test_id')
                            ->groupBy('test.id')
                            ->asArray()
                            ->one();
                        if (!$test) {
                            $test['count'] = 0;
                        }

                        $ticket->ball = $test['count'];
                        $ticket->save(false);
                        return true;
                    }
                }
            }
            $this->error_400();
        }
        $this->error_400();
    }

    // MyResult
    public function actionMyresult()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ticket_id = $this->cleartext(Yii::$app->request->get('ticket_id'));
        $type = Yii::$app->request->get('type');
        $data = Yii::$app->request->getRawBody();
        $data = json_decode($data);
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if (Yii::$app->request->isGet) {
            if ($ticket_id) {
                $this->validation_id('ticket', $ticket_id);
                $ticket = Ticket::find()
                    ->where(['ticket.status' => 1])
                    ->andWhere(['ticket.id' => $ticket_id])
                    ->one();
                if ($ticket) {
                    return $ticket->result();
                }
                $this->error_400();
            }
        }
        if ($this->ActiveToken($token)) {
            if (Yii::$app->request->isGet) {
                $student = Profile::find()
                    ->where(['user_id' => $this->ActiveToken($token)])
                    ->one();
                $ticket_passives = Ticket::find()
                    ->where(['student_id' => $student->id])
                    ->andWhere(['status' => 0])
                    ->andWhere(['>', 'type', '0'])
                    ->all();
                if ($ticket_passives) {
                    foreach ($ticket_passives as $ticket_passive) {
                        $this->check_test($token, $ticket_passive->type);
                    }
                }
                $tickets = Ticket::find()
                    ->where(['ticket.student_id' => $student->id])
                    ->andWhere(['ticket.status' => 1])
                    ->orderBy(['id' => SORT_DESC])
                    ->all();
                if ($type) {
                    if ($type == 3) {
                        $type = 0;
                    }
                    $tickets = Ticket::find()
                        ->where(['ticket.student_id' => $student->id])
                        ->andWhere(['ticket.status' => 1])
                        ->andWhere(['ticket.type' => $type])
                        ->orderBy(['id' => SORT_DESC])
                        ->all();
                }
                $result = [];
                if ($tickets) {
                    foreach ($tickets as $ticket) {
                        $i = 1;
                        $ball = 0;
                        $res = (object) [];
                        $resul = [];
                        $test_ids = explode(',', $ticket->test_ids);
                        $count = count($test_ids);
                        if ($ticket->type == 2 && $count < 4) {
                            $res->ticket_id = $ticket->id;
                            $res->type = 'DTM';
                            $res->date = 1000 * (int) $ticket->created_at;
                            $i = 1;
                            foreach ($test_ids as $test_id) {
                                $dtm = (object) [];
                                $subject = new Subject();
                                if ($subject->test($test_id)) {
                                    $dtm->name = $subject->test($test_id)->name;
                                } else {
                                    $dtm->name = 'fan';
                                }
                                $blok = 'blok_' . $i;
                                $dtm->ball = $ticket->$blok;
                                $resul[] = $dtm;
                                $i++;
                            }
                            $res->percent = round(
                                (100 * $ticket->ball) / 189,
                                1
                            );
                            if ($count == 1) {
                                $res->percent = round(
                                    (100 * $ticket->ball) / 93,
                                    1
                                );
                            }
                            $res->info = $resul;
                        } elseif ($ticket->type == 1 || $count > 3) {
                            $res->ticket_id = $ticket->id;
                            $res->type = 'Virtual';
                            if ($ticket->type == 2) {
                                $res->type = 'DTM';
                            }
                            $res->date = 1000 * (int) $ticket->created_at;
                            foreach ($test_ids as $test_id) {
                                $dtm = (object) [];
                                $subject = new Subject();
                                if ($subject->test($test_id)) {
                                    $dtm->name = $subject->test($test_id)->name;
                                } else {
                                    $dtm->name = 'fan';
                                }
                                $blok = 'blok_' . $i;
                                $dtm->ball = $ticket->$blok;
                                $resul[] = $dtm;
                                $i++;
                            }
                            $res->percent = round(
                                (100 * $ticket->ball) / 189,
                                1
                            );
                            $res->info = $resul;
                        } elseif ($ticket->type == 0) {
                            $res->ticket_id = $ticket->id;
                            $res->type = 'Mavzuli';
                            $res->date = 1000 * (int) $ticket->created_at;
                            foreach ($test_ids as $test_id) {
                                $dtm = (object) [];
                                $subject = new Subject();
                                if ($subject->test($test_id)) {
                                    $dtm->name = $subject->test($test_id)->name;
                                } else {
                                    $dtm->name = 'fan';
                                }
                                $blok = 'blok_' . $i;
                                $dtm->ball = $ticket->$blok;
                                $resul[] = $dtm;
                                $i++;
                            }
                            if ($ball == 0) {
                                $res->percent = 0;
                            } else {
                                $res->percent = round(
                                    (100 * $ticket->ball) / $ball,
                                    1
                                );
                            }
                            $res->info = $resul;
                        }
                        $result[] = $res;
                    }
                }
                $response = [
                    'result' => $result,
                ];
                return $response;
            }
            if (Yii::$app->request->isPost) {
                $student = Profile::find()
                    ->where(['user_id' => $this->ActiveToken($token)])
                    ->one();
                if ($student) {
                    if (isset($data->id)) {
                        $this->validation_id('ticket', $data->id);
                        $ticket = Ticket::find()
                            ->where(['student_id' => $student->id])
                            ->andWhere(['id' => $data->id])
                            ->one();
                        if ($ticket->begin_answer_id > 0) {
                            $min_id = $ticket->begin_answer_id;
                            $max_id = $ticket->begin_answer_id + 150;
                        } else {
                            $min_id =
                                UserAnswer::find()
                                // ->select('min(id)')
                                ->where(['ticket_id' => $ticket->id])
                                ->min('id') + 1;

                            $max_id =
                                UserAnswer::find()
                                // ->select('min(id)')
                                ->where(['ticket_id' => $ticket->id])
                                ->max('id') + 1;
                        }
                        if ($ticket) {
                            $user_answers = UserAnswer::find()
                                ->where(['>', 'user_answer.id', $min_id])
                                ->andWhere(['<', 'user_answer.id', $max_id])
                                ->andWhere(['ticket_id' => $ticket->id])
                                ->all();
                            foreach ($user_answers as $user_ans) {
                                $user_ans->delete();
                            }
                            $ticket->delete();
                            return true;
                        }
                        $this->error_403();
                    }
                    $tickets = Ticket::find()
                        ->where(['student_id' => $student->id])
                        ->all();
                    if ($tickets) {
                        foreach ($tickets as $ticket) {
                            UserAnswer::deleteAll(['ticket_id' => $ticket->id]);
                            $ticket->delete();
                        }
                    }
                    return true;
                }
                $this->error_403();
            }
            $this->error_400();
        }
        $this->error(400, "Ro'yhatdan o'ting");
    }
    public function actionCopylink()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ticket_id = $this->cleartext(Yii::$app->request->get('ticket_id'));
        $this->validation_id('ticket', $ticket_id);

        $ticket = Ticket::findOne($ticket_id);
        $name = $ticket->student_id;

        $font = dirname(__DIR__) . '/web/BEBAS.ttf';
        $imageURL = 'upload/image/video.jpg';
        $img = imagecreatefromjpeg($imageURL);
        $textcolor = imagecolorallocate($img, 255, 209, 102);

        $bar = Yii::$app->security->generateRandomString() . '.png';
        $filename = sprintf('upload/image/' . $bar);
        $width = imagesx($img);
        $height = imagesy($img);
        // THE TEXT SIZE
        $val = 50;
        if (strlen($name) > 200) {
            $val = strlen($name) / 4;
        }
        $mas = explode(' ', $name);

        $json = [];
        $count = 0;
        $text = '';
        foreach ($mas as $key => $ma) {
            $count += strlen($ma) + 1;
            $text .= $ma . ' ';
            if ($key == max(array_keys($mas))) {
                $json[] = $text;
                $text = '';
                $count = 0;
            }
            if ($count >= $val) {
                $json[] = $text;
                $text = '';
                $count = 0;
            }
        }
        $count_array = count($json);

        if ($count_array == 1) {
            $text_size = imagettfbbox(24, 0, $font, $name);
            $text_width =
                max([$text_size[2], $text_size[4]]) -
                min([$text_size[0], $text_size[6]]);
            $text_height =
                max([$text_size[5], $text_size[7]]) -
                min([$text_size[1], $text_size[3]]);
            $text_height = 0;
            // CENTERING THE TEXT BLOCK
            $centerX = CEIL(($width - $text_width) / 2);
            $centerX = $centerX < 0 ? 0 : $centerX;
            $centerY = CEIL(($height - $text_height) / 2);
            $centerY = $centerY < 0 ? 0 : $centerY;
            imagettftext(
                $img,
                24,
                0,
                $centerX,
                $centerY,
                $textcolor,
                $font,
                $name
            );
        } elseif ($count_array == 2) {
            $text_size = imagettfbbox(24, 0, $font, $json[0]);
            $text_width =
                max([$text_size[2], $text_size[4]]) -
                min([$text_size[0], $text_size[6]]);
            $text_height =
                max([$text_size[5], $text_size[7]]) -
                min([$text_size[1], $text_size[3]]);
            $text_height = 40;
            // CENTERING THE TEXT BLOCK
            $centerX = CEIL(($width - $text_width) / 2);
            $centerX = $centerX < 0 ? 0 : $centerX;
            $centerY = CEIL(($height - $text_height) / 2);
            $centerY = $centerY < 0 ? 0 : $centerY;
            imagettftext(
                $img,
                24,
                0,
                $centerX,
                $centerY,
                $textcolor,
                $font,
                $json[0]
            );

            $text_size = imagettfbbox(24, 0, $font, $json[1]);
            $text_width =
                max([$text_size[2], $text_size[4]]) -
                min([$text_size[0], $text_size[6]]);
            $text_height =
                max([$text_size[5], $text_size[7]]) -
                min([$text_size[1], $text_size[3]]);
            $text_height = -40;
            // CENTERING THE TEXT BLOCK
            $centerX = CEIL(($width - $text_width) / 2);
            $centerX = $centerX < 0 ? 0 : $centerX;
            $centerY = CEIL(($height - $text_height) / 2);
            $centerY = $centerY < 0 ? 0 : $centerY;
            imagettftext(
                $img,
                24,
                0,
                $centerX,
                $centerY,
                $textcolor,
                $font,
                $json[1]
            );
        }
        imagepng($img, $filename);
    }
    // Saved test
    public function actionSavedtest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if ($this->ActiveToken($token)) {
            if (Yii::$app->request->isGet) {
                $json = [];
                $student = Profile::find()
                    ->where(['user_id' => $this->ActiveToken($token)])
                    ->one();
                $test_ids = explode(',', $student->test);
                $lessons = Subject::find()
                    ->select(
                        'COUNT(ticket.test_ids) as count, subject.name as subject_name,test.create_date, lesson.name as lesson_name, test.id as test_id,'
                    )
                    ->where(['test.status' => 1])
                    ->andWhere(['lesson.status' => 1])
                    ->andWhere(['subject.status' => 1])
                    ->andWhere(['in', 'test.id', $test_ids])
                    ->leftJoin('lesson', 'subject.id=lesson.subject_id')
                    ->leftJoin('test', 'test.lesson_id=lesson.id')
                    ->leftJoin('ticket', 'test.id=ticket.test_ids')
                    ->groupBy('test.id')
                    ->asArray()
                    ->all();
                if ($lessons) {
                    foreach ($lessons as $lesson) {
                        $object = (object) [];
                        $count = Question::find()
                            ->where(['test_id' => (int) $lesson['test_id']])
                            ->andWhere(['status' => 1])
                            ->count();
                        $object->subject_name = $lesson['subject_name'];
                        $object->lesson_name = $lesson['lesson_name'];
                        $object->create_date = $lesson['create_date'];
                        $object->test_id = (int) $lesson['test_id'];
                        $object->test_count = $count;
                        $object->view_count = (int) $lesson['count'];
                        $object->is_saved = true;
                        $json[] = $object;
                    }
                }
                return $json;
            }
            $this->error_400();
        }
        $this->error_400();
    }
    // Registratsiya
    public function actionSendsms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        // if(Yii::$app->request->isGet){
        //     $image = new CaptchaHelper();
        //     $images = $image->generateImage();
        //     return $images;
        // }
        if (Yii::$app->request->isPost) {
            $user_data = Yii::$app->request->getRawBody();
            $user_data = json_decode($user_data);
            $ip = Yii::$app->request->getUserIP();
            $user_ip = UserIp::find()
                ->where(['ip' => $ip])
                ->one();
            if ($user_ip) {
                if ($user_ip->time > time()) {
                    $this->error(401, "Keyinroq urinib ko'ring");
                }
                if (time() - $user_ip->time < 1) {
                    $user_ip->time = time() + 600;
                    $user_ip->save(false);
                    $this->error(401, "Keyinroq urinib ko'ring");
                }
                if (time() - $user_ip->time > 10) {
                    $user_ip->time = time();
                    $user_ip->save(false);
                    $phone = $this->cleartext($user_data->phone);
                    $AuthorizationKey = 'YWltU01TOkFAaTJObSEyMzROZHM=';
                    $userTelFULL = $phone;
                    $MSGRecipient = $userTelFULL;
                    $userConfCode = rand(11111, 99999);

                    $MSGID = date('Salom') . '' . rand(111, 999); //generateID like '191009102641261' -->> 19-10-09 10:26:41 rand:261
                    $MSGContent =
                        'test.aim.uz Tasdiqlash kodi:' .
                        $userConfCode .
                        ' Login:' .
                        $userTelFULL;

                    $curl = curl_init();

                    curl_setopt_array($curl, [
                        CURLOPT_URL => 'http://91.204.239.44/broker-api/send',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => "{ 
                            \"messages\": 
                            [{
                            \"recipient\": \"$MSGRecipient\",
                            \"message-id\": \"$MSGID\",
                            \"sms\": 
                            {
                                \"originator\": \"3700\",
                                \"content\": 
                                { 
                                    \"text\": \"$MSGContent\" 
                                } 
                            } 
                            }]
                            }",
                        CURLOPT_HTTPHEADER => [
                            'Authorization: Basic ' . $AuthorizationKey . '',
                            'Cache-Control: no-cache',
                            'Content-Type: application/json',
                        ],
                    ]);

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        $response = [
                            'message' => 'cURL Error #:' . $err,
                        ];
                        return $response;
                    } elseif ($response == 'Request is received') {
                        $login = Login::find()
                            ->where(['phone' => substr($phone, -9)])
                            ->one();
                        if ($login) {
                            $login->code = $userConfCode;
                            $login->status = 0;
                            $login->save(false);
                            $response = [
                                'message' => true,
                            ];
                            return $response;
                        }
                        $login = new Login();
                        $login->phone = substr($phone, -9);
                        $login->code = $userConfCode;
                        $login->save(false);

                        $response = [
                            'message' => true,
                        ];
                        return $response;
                    } else {
                        $response = [
                            'message' => false,
                        ];
                        return $response;
                    }
                }
                $this->error(401, "Keyinroq urinib ko'ring");
            } else {
                $user_ip = new UserIp();
                $user_ip->ip = $ip;
                $user_ip->time = time();
                $user_ip->save(false);
                $phone = $this->cleartext($user_data->phone);
                $AuthorizationKey = 'YWltU01TOkFAaTJObSEyMzROZHM=';
                $userTelFULL = $phone;
                $MSGRecipient = $userTelFULL;
                $userConfCode = rand(11111, 99999);

                $MSGID = date('Salom') . '' . rand(111, 999); //generateID like '191009102641261' -->> 19-10-09 10:26:41 rand:261
                $MSGContent =
                    'test.aim.uz Tasdiqlash kodi:' .
                    $userConfCode .
                    ' Login:' .
                    $userTelFULL;

                $curl = curl_init();

                curl_setopt_array($curl, [
                    CURLOPT_URL => 'http://91.204.239.44/broker-api/send',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => "{ 
                        \"messages\": 
                        [{
                        \"recipient\": \"$MSGRecipient\",
                        \"message-id\": \"$MSGID\",
                        \"sms\": 
                        {
                            \"originator\": \"3700\",
                            \"content\": 
                            { 
                                \"text\": \"$MSGContent\" 
                            } 
                        } 
                        }]
                        }",
                    CURLOPT_HTTPHEADER => [
                        'Authorization: Basic ' . $AuthorizationKey . '',
                        'Cache-Control: no-cache',
                        'Content-Type: application/json',
                    ],
                ]);

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    $response = [
                        'message' => 'cURL Error #:' . $err,
                    ];
                    return $response;
                } elseif ($response == 'Request is received') {
                    $login = Login::find()
                        ->where(['phone' => substr($phone, -9)])
                        ->one();
                    if ($login) {
                        $login->code = $userConfCode;
                        $login->status = 0;
                        $login->save(false);
                        $response = [
                            'message' => true,
                        ];
                        return $response;
                    }
                    $login = new Login();
                    $login->phone = substr($phone, -9);
                    $login->code = $userConfCode;
                    $login->save(false);

                    $response = [
                        'message' => true,
                    ];
                    return $response;
                } else {
                    $response = [
                        'message' => false,
                    ];
                    return $response;
                }
            }
            // $code = $user_data->code;
            // $image = new CaptchaHelper();
            // if (!$image->verify($code)) {
            //     $response = [
            //         'message' => "Xatolik"
            //     ];
            //     return $response;
            // }

            // if(\xstreamka\recaptcha\ReCaptcha::validate()){
            //     return true;
            // }
            // return false;
        }
        $this->error_400();
    }
    public function actionChecksms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $token = Yii::$app
                ->getRequest()
                ->getCookies()
                ->getValue('token');
            $user_data = Yii::$app->request->getRawBody();
            $user_data = json_decode($user_data);
            $code = $this->cleartext($user_data->code);
            $phone = $this->cleartext($user_data->phone);
            $username = substr($phone, -9);
            $login = Login::find()
                ->where(['phone' => $username])
                ->andWhere(['status' => 0])
                ->one();
            if ($login) {
                if ($login->code == $code) {
                    $login->status = 1;
                    $login->save(false);
                    $user = User::find()
                        ->where(['username' => $username])
                        ->andWhere(['status' => 1])
                        ->one();
                    if ($user) {
                        // $user->token = bin2hex(random_bytes(32));
                        $student = Profile::find()
                            ->where(['user_id' => $user->id])
                            ->one();
                        $json = (object) [];
                        $json->id = $user->id;
                        $json->name = $student->full_name;
                        $json->token = $user->token;
                        $json->role = $user->role_id;
                        $json->phone = '+998' . $user->username;
                        if ($student->birthday) {
                            $json->birthday = $student->birthday;
                        }
                        if ($student->gender) {
                            $json->gender = $student->gender;
                        }
                        if ($student->email) {
                            $json->email = $student->email;
                        }
                        if ($student->address_id) {
                            $json->address = $student->address_id;
                        }
                        // if ($user->save(false)) {
                        $cookies = Yii::$app->response->cookies;
                        $cookies->add(
                            new \yii\web\Cookie([
                                'domain' => '.aim.uz',
                                'name' => 'token',
                                'value' => $user->token,
                                'sameSite' => 'none',
                                'secure' => true,
                            ])
                        );
                        $response = [
                            'message' => true,
                            'user' => $json,
                        ];
                        return $response;
                        // }
                        $this->error_400();
                    }
                    if (isset($token) && $token != '') {
                        $user = User::find()
                            ->where(['token' => $token])
                            ->andWhere(['status' => 0])
                            ->one();
                        if ($user) {
                            $user->username = $username;
                            $user->token = bin2hex(random_bytes(32));
                            $user->image = 'student.png';
                            if ($user->save(false)) {
                                $cookies = Yii::$app->response->cookies;
                                $cookies->add(
                                    new \yii\web\Cookie([
                                        'domain' => '.aim.uz',
                                        'name' => 'token',
                                        'value' => $user->token,
                                        'sameSite' => 'none',
                                        'secure' => true,
                                    ])
                                );
                                $json = (object) [];
                                $response = [
                                    'message' => false,
                                    'user' => $user,
                                ];
                                return $response;
                            }
                        }
                    }
                    $user = User::find()
                        ->select('id,image,role_id,token,username')
                        ->where(['username' => $username])
                        ->andWhere(['status' => 0])
                        ->one();
                    if ($user) {
                        $cookies = Yii::$app->response->cookies;
                        $cookies->add(
                            new \yii\web\Cookie([
                                'domain' => '.aim.uz',
                                'name' => 'token',
                                'value' => $user->token,
                                'sameSite' => 'none',
                                'secure' => true,
                            ])
                        );
                        $response = [
                            'message' => false,
                            'user' => $user,
                        ];
                        return $response;
                    }
                    $user = new User();
                    $user->username = $username;
                    $user->setPassword($code);
                    $user->status = 0;
                    $user->token = bin2hex(random_bytes(32));
                    $user->auth_key = $user->token;
                    $user->image = 'student.png';
                    if ($user->save(false)) {
                        $cookies = Yii::$app->response->cookies;
                        $cookies->add(
                            new \yii\web\Cookie([
                                'domain' => '.aim.uz',
                                'name' => 'token',
                                'value' => $user->token,
                                'sameSite' => 'none',
                                'secure' => true,
                            ])
                        );
                        $response = [
                            'message' => false,
                            'user' => $user,
                        ];
                        return $response;
                    }
                }
                $this->error(400, 'Kod xato qayta tekshiring');
            }
        }
        $this->error_400();
    }
    public function actionProfile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');

        if (Yii::$app->request->isPost) {
            $user_data = Yii::$app->request->getRawBody();
            $user_data = json_decode($user_data);
            if ($this->Token($token) && isset($user_data->full_name)) {
                $full_name = $this->cleartext($user_data->full_name);
                $role = $this->cleartext($user_data->role);
                if ($role == 1) {
                    $teacher = Profile::find()
                        ->where(['user_id' => $this->ActiveToken($token)])
                        ->one();
                    if ($teacher) {
                        $user = User::findOne($teacher->user_id);
                        $json = (object) [];
                        $json->id = $user->id;
                        $json->name = $teacher->full_name;
                        $json->token = $user->token;
                        $json->role = $user->role_id;
                        $json->phone = '+998' . $user->username;
                        $response = [
                            'user' => $json,
                        ];
                        return $response;
                    }
                    $teacher = Profile::find()
                        ->where(['user_id' => $this->Token($token)])
                        ->one();
                    if ($teacher) {
                        $teacher->user_id = $this->Token($token);
                        $teacher->full_name = $full_name;
                        if ($teacher->save(false)) {
                            $user = User::findOne($this->Token($token));
                            $user->status = 1;
                            $user->role_id = 4;
                            $user->save(false);
                            $json = (object) [];
                            $json->id = $user->id;
                            $json->name = $teacher->full_name;
                            $json->token = $user->token;
                            $json->role = $user->role_id;
                            $json->phone = '+998' . $user->username;
                            $response = [
                                'user' => $json,
                            ];
                            return $response;
                        }
                    }
                    $teacher = new Profile();
                    $teacher->user_id = $this->Token($token);
                    $teacher->full_name = $full_name;
                    if ($teacher->save(false)) {
                        $user = User::findOne($this->Token($token));
                        $user->status = 1;
                        $user->role_id = 4;
                        $user->save(false);
                        $json = (object) [];
                        $json->id = $user->id;
                        $json->name = $teacher->full_name;
                        $json->token = $user->token;
                        $json->role = $user->role_id;
                        $json->phone = '+998' . $user->username;
                        $response = [
                            'user' => $json,
                        ];
                        return $response;
                    }
                } elseif ($role == 0) {
                    $student = Profile::find()
                        ->where(['user_id' => $this->ActiveToken($token)])
                        ->one();
                    if ($student) {
                        $user = User::findOne($student->user_id);
                        $json = (object) [];
                        $json->id = $user->id;
                        $json->name = $student->full_name;
                        $json->token = $user->token;
                        $json->role = $user->role_id;
                        $json->phone = '+998' . $user->username;
                        $response = [
                            'user' => $json,
                        ];
                        return $response;
                    }
                    $student = Profile::find()
                        ->where(['user_id' => $this->Token($token)])
                        ->one();
                    if ($student) {
                        $student->user_id = $this->Token($token);
                        $student->full_name = $full_name;
                        if ($student->save(false)) {
                            if (isset($user_data->link)) {
                                $group = Group::findOne(['link' => $user_data->link]);
                                if ($group) {
                                    $old_group = StudentGroup::find()
                                        ->where(['profile_id' => $student->id])
                                        ->andWhere(['group_id' => $group->id])
                                        ->andWhere(['!=', 'status', StudentGroup::DELETED])
                                        ->one();
                                    if (!$old_group) {
                                        $studentt = new StudentGroup();
                                        $studentt->name = $student->full_name;
                                        $studentt->group_id = $group->id;
                                        $studentt->profile_id = $student->id;
                                        $studentt->join_in = time();
                                        $studentt->status = StudentGroup::ACTIVE;
                                        $studentt->save();
                                    }
                                }
                            }
                            $user = User::findOne($this->Token($token));
                            $user->status = 1;
                            $user->role_id = 5;
                            $user->save(false);
                            $json = (object) [];
                            $json->id = $user->id;
                            $json->name = $student->full_name;
                            $json->token = $user->token;
                            $json->role = $user->role_id;
                            $json->phone = '+998' . $user->username;
                            $response = [
                                'user' => $json,
                            ];
                            return $response;
                        }
                    }
                    $student = new Profile();
                    $student->user_id = $this->Token($token);
                    $student->full_name = $full_name;
                    if ($student->save(false)) {
                        if (isset($user_data->link)) {
                            $group = Group::findOne(['link' => $user_data->link]);
                            if ($group) {
                                $old_group = StudentGroup::find()
                                    ->where(['profile_id' => $student->id])
                                    ->andWhere(['group_id' => $group->id])
                                    ->andWhere(['!=', 'status', StudentGroup::DELETED])
                                    ->one();
                                if (!$old_group) {
                                    $studentt = new StudentGroup();
                                    $studentt->name = $student->full_name;
                                    $studentt->group_id = $group->id;
                                    $studentt->profile_id = $student->id;
                                    $studentt->join_in = time();
                                    $studentt->status = StudentGroup::ACTIVE;
                                    $studentt->save();
                                }
                            }
                        }
                        $user = User::findOne($this->Token($token));
                        $user->status = 1;
                        $user->role_id = 5;
                        $user->save(false);
                        $json = (object) [];
                        $json->id = $user->id;
                        $json->name = $student->full_name;
                        $json->token = $user->token;
                        $json->role = $user->role_id;
                        $json->phone = '+998' . $user->username;
                        $response = [
                            'user' => $json,
                        ];
                        return $response;
                    }
                }
            }
            $this->error_403();
        }
        $this->error_400();
    }
    // MyPage
    public function actionAuth()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if ($this->ActiveToken($token)) {
            return true;
        }
        return false;
    }
    public function actionMypage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if ($this->ActiveToken($token)) {
            if (Yii::$app->request->isGet) {
                $user = User::findOne($this->ActiveToken($token));
                $student = Profile::find()
                    ->where(['user_id' => $user->id])
                    ->one();
                $json = (object) [];
                $json->name = $student->full_name;
                $json->phone = '+998' . $user->username;
                $json->role = $user->role_id;
                $json->image = URL_USER_IMAGE . $user->image;
                if ($student->birthday) {
                    $json->birthday = $student->birthday;
                }
                if ($student->gender) {
                    $json->gender = $student->gender;
                }
                if ($student->email) {
                    $json->email = $student->email;
                }
                if ($user->role_id == 5) {
                    $top_user = Ticket::find()
                        ->select('SUM(ticket.ball) as ball')
                        ->leftJoin('profile', 'profile.id=ticket.student_id')
                        ->leftJoin('user', 'user.id=profile.user_id')
                        ->where(['>', 'ball', 0])
                        ->andWhere(['user.id' => $user->id])
                        ->orderBy(['ball' => SORT_DESC])
                        ->groupBy('user.id')
                        ->asArray()
                        ->one();
                    $json->ball = round($top_user['ball'], 1);
                }
                if ($student->address_id) {
                    $district = Address::find()
                        ->where(['id' => $student->address_id])
                        ->one();
                    $region = Address::findOne($district->parent_id);
                    $json->region = $region;
                    $json->district = $district;
                }
                return $json;
            } elseif (Yii::$app->request->isPost) {
                $data = json_decode($_POST['data']);
                $student = Profile::find()
                    ->where(['user_id' => $this->ActiveToken($token)])
                    ->one();
                if (isset($data->name)) {
                    $student->full_name = $this->cleartext($data->name);
                }
                if (isset($data->birthday)) {
                    $student->birthday = $this->cleartext($data->birthday);
                }
                if (isset($data->gender)) {
                    $student->gender = $this->cleartext($data->gender);
                }
                if (isset($data->email)) {
                    $student->email = $this->cleartext($data->email);
                }
                if (isset($data->address_id)) {
                    $student->address_id = $this->cleartext($data->address_id);
                }
                if (isset($_FILES['image'])) {
                    $user = User::findOne($student->user_id);
                    $path = $_FILES['image']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);

                    $target_dir = 'img/';

                    $newFileName =
                        Yii::$app->security->generateRandomString() .
                        '.' .
                        $ext;
                    $uploadPath = $target_dir . $newFileName;

                    $maxFileSize = 2 * 1024 * 1024 * 8; //  20MB

                    if (
                        ($ext == 'png' ||
                            $ext == 'jpg' ||
                            $ext == 'jpeg' ||
                            $ext == 'PNG' ||
                            $ext == 'JPG' ||
                            $ext == 'JPEG') &&
                        $_FILES['image']['size'] < $maxFileSize
                    ) {
                        if (
                            move_uploaded_file(
                                $_FILES['image']['tmp_name'],
                                $uploadPath
                            )
                        ) {
                            $user->image = $newFileName;
                            $user->save(false);
                        }
                    }
                }
                if ($student->gender == 'ayol') {
                    $user = User::findOne($student->user_id);
                    if ($user->image == 'student.png') {
                        $user->image = 'ayol.jpg';
                        $user->save(false);
                    }
                }
                if ($student->gender == 'erkak') {
                    $user = User::findOne($student->user_id);
                    if ($user->image == 'ayol.jpg') {
                        $user->image = 'student.png';
                        $user->save(false);
                    }
                }
                $student->save(false);
                return true;
            }
            $this->error_400();
        } elseif ($this->Token($token)) {
            $json = (object) [];
            return $json;
        }
        $this->error_401();
    }
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = $this->cleartext(Yii::$app->request->get('id'));
        if (Yii::$app->request->isGet) {
            if ($id) {
                $regions = Address::find()
                    ->select('id,name')
                    ->where(['parent_id' => $id])
                    ->all();
                $json = [];
                foreach ($regions as $region) {
                    $object = (object) [];
                    $object->value = $region->id;
                    $object->label = $region->name;
                    $json[] = $object;
                }
                return $json;
            }
            $regions = Address::find()
                ->select('id,name')
                ->where(['parent_id' => null])
                ->all();
            $json = [];
            foreach ($regions as $region) {
                $object = (object) [];
                $object->value = $region->id;
                $object->label = $region->name;
                $json[] = $object;
            }
            return $json;
        }
        $this->error_400();
    }
    public function actionLogout()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->response->cookies;
        $cookies->add(
            new \yii\web\Cookie([
                'domain' => '.aim.uz',
                'name' => 'token',
                'value' => '',
                'sameSite' => 'none',
                'secure' => true,
            ])
        );
        return true;
    }
    public function actionLogerror()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        $data = Yii::$app->request->getRawBody();
        $data = json_decode($data);
        if (Yii::$app->request->isPost) {
            $phone = $token;
            if (isset($token) && $token != '') {
                $user = User::findOne($this->Activetoken($token));
                if ($user) {
                    $phone = $user->username;
                    $current = file_get_contents('error.txt');
                    $current =
                        'Sana: ' .
                        date('H:i:s d-m-Y', time()) .
                        "\n" .
                        "Number: $phone\n" .
                        "Page: $data->page\n" .
                        "Error: $data->error\n\n" .
                        $current;
                    file_put_contents('error.txt', $current);

                    return true;
                }
                $current = file_get_contents('error.txt');
                $current =
                    'Sana: ' .
                    date('H:i:s d-m-Y', time()) .
                    "\n" .
                    "Token: $phone\n" .
                    "Page: $data->page\n" .
                    "Error: $data->error\n\n" .
                    $current;
                file_put_contents('error.txt', $current);

                return true;
            }
            $current = file_get_contents('error.txt');
            $current =
                'Sana: ' .
                date('H:i:s d-m-Y', time()) .
                "\n" .
                "Token: $phone\n" .
                "Page: $data->page\n" .
                "Error: $data->error\n\n" .
                $current;
            file_put_contents('error.txt', $current);
            return true;
        }
        $this->error_400();
    }

    // Seo
    public function actionSeo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $type = Yii::$app->request->get('type');
        $val = Yii::$app->request->get('val');
        if (Yii::$app->request->isGet) {
            if ($type == 'page') {
                $seo = Seo::find()
                    ->select('meta,description')
                    ->where(['page' => $val])
                    ->one();
                if (!$seo) {
                    $seo = (object) [];
                    $seo->mata = '';
                    $seo->description = '';
                }
                return $seo;
            } elseif ($type == 'subject') {
                $subject = Subject::find()
                    ->where(['name' => "$val"])
                    ->one();

                if (!$subject) {
                    $this->error_400();
                }

                $seo = Seo::find()
                    ->select('meta,description')
                    ->where(['subject_id' => $subject->id])
                    ->one();

                if (!$seo) {
                    $seo = (object) [];
                    $seo->mata = '';
                    $seo->description = '';
                }

                return $seo;
            }
        }
        $this->error_400();
    }

    // Student interesting subject
    public function actionInteresting()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $users = Subject::find()
            ->select(
                'ticket.student_id as student_id,subject.name as name,subject.id as subject_id,(ticket.finished_at-ticket.created_at) as time'
            )
            ->leftJoin('test', 'test.subject_id=subject.id')
            ->leftJoin('ticket', 'test.id=ticket.test_ids')
            ->where(['>', 'ticket.ball', 0])
            ->where(['ticket.type' => 0])
            ->having(['>', 'time', 200])
            ->orderBy(['ticket.student_id' => SORT_ASC])
            ->asArray()
            ->all();

        $massiv_students = $this->array_group_by($users, 'student_id');

        $json = [];

        foreach ($massiv_students as $key => $val) {
            $min = 0;

            $massiv_subjects = $this->array_group_by($val, 'subject_id');

            if ($massiv_subjects) {
                foreach ($massiv_subjects as $keyy => $value) {
                    if ($min < count($value)) {
                        $min = count($value);
                        // $max_count = count($value);
                        $subject_id = $key;
                    }
                }
                $object = (object) [];
                $object->student_id = $key;
                $object->subject_id = $subject_id;
                $json[] = $object;
            }
        }

        $file = fopen('interesting.json', 'w');

        // $file = json_decode(file_get_contents('interesting.json'));  o'qish uchun

        fwrite($file, json_encode($json, JSON_PRETTY_PRINT));

        return $json;
    }

    // Site questions

    public function actionSitequestion()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app
            ->getRequest()
            ->getCookies()
            ->getValue('token');
        if ($this->ActiveToken($token)) {
            $answer = SiteAnswer::find()
                ->select('site_answer.site_question_id as id')
                ->leftJoin('profile', 'profile.id = site_answer.profile_id')
                ->leftJoin('user', 'user.id = profile.user_id')
                ->where(['user.token' => $token])
                ->asArray()
                ->get();

            $ids = ArrayHelper::getColumn($answer, function ($element) {
                return $element['id'];
            });

            $question = SiteQuestion::find()
                ->select('id,name')
                ->where(['NOT IN', 'id', $ids])
                ->one();
            if ($question) {
                $option = SiteOption::find()
                    ->select('id,name')
                    ->where(['site_question_id' => $question->id])
                    ->all();

                return [
                    'question' => $question,
                    'option' => $option,
                ];
            }
        }
    }

    
}
