<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    // kirishda qayerga 1-borishi
    // 'defaultRoute' =>'post/salom',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation' => false,
        ],
        'reCaptcha' => [
            'class' => 'xstreamka\recaptcha\ReCaptchaConfig',
            'siteKeyV3' => '6Le4e0gbAAAAAFyIHzWmX5Nb0xz9Yq_ao6tSncWF',
            'secretV3' => '6Le4e0gbAAAAAA8RcGXiJihN15B-UUjQQI956AQ2',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-frontend',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'sergeymakinen\yii\telegramlog\Target',
                    'token' => '5035255897:AAHmged7Hyx0LYyaFmS9jFI9LUXw2hJIJ8Y',
                    'chatId' => 150407024,
                    'levels'=>1,
                    // 'template' =>'category'
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'scriptUrl' => '/index.php',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
    ],
    'params' => $params,
];
