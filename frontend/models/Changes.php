<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "changes".
 *
 * @property int $id
 * @property int $user_id
 * @property int $object_id
 * @property int $action 1 => create 2=> update 0 => delete
 * @property int $table
 * @property int|null $commit
 * @property string $time
 */
class Changes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'changes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id', 'action', 'table', 'time'], 'required'],
            [['user_id', 'object_id', 'action', 'table', 'commit'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'object_id' => 'Object ID',
            'action' => 'Action',
            'table' => 'Table',
            'commit' => 'Commit',
            'time' => 'Time',
        ];
    }
}
