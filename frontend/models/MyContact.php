<?php
 namespace frontend\models;

use Symfony\Contracts\Service\Attribute\Required;
use Yii;
use yii\base\Model;

  class MyContact extends Model{
      public $malumot_1;
      public $malumot_2;

      public function rules()
      {
       return [
           [['malumot_1','malumot_2'], 'required'],
           ['malumot_1','in','range'=>range(7,30)],
       ];    
      }
  }
?>